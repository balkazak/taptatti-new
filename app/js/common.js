$(function() {

	// Custom JS

    $(window).scroll(function (){
        var scroll = $(window).scrollTop();

        if (scroll >=20) {
            $('.nav_scroll').css('top', '200px');
        }
        else {
            $('.nav_scroll').css('top', '-10px');
        }
    });

    var app = new Vue({
        el: '#tapTatti',
        data(){
            return{
                userMenu: false,
                cartMenu: false,
                modalProduct: null,
                modalWindow: 1,
                showModal: false,
                sections: [
                    {
                        heading: 'Торты',
                        filters:[
                            {
                                name:'Сливочный',
                                type: 1
                            },
                            {
                                name:'Шоколадный',
                                type: 2
                            },
                            {
                                name:'Творожный',
                                type: 3
                            },
                            {
                                name:'Новинки',
                                type: 4
                            },
                            {
                                name:'Хит',
                                type: 5
                            },
                            {
                                name:'Без маргарина',
                                type: 6
                            },
                            {
                                name:'Низкокалорийный',
                                type: 7
                            },
                            {
                                name:'Натуральные сливочные',
                                type: 8
                            }
                        ],
                        products:[
                            {
                                img: '/assets/images/products/torts/product.png',
                                name: 'Молочная девочка',
                                price: 9000,
                                count: 1,
                                flags: ['Хит']
                            },
                            {
                                img: '/assets/images/products/torts/product1.png',
                                name: 'Whoopie',
                                price: 4000,
                                count: 1,
                                flags: []
                            },
                            {
                                img: '/assets/images/products/torts/product2.png',
                                name: 'Медовый',
                                price: 5000,
                                count: 1,
                                flags: ['Новинка']
                            }
                        ]
                    },
                    {
                        heading: 'Пироги',
                        filters:[
                            {
                                name:'С курицей',
                                type: 1
                            },
                            {
                                name:'Вегетерианский',
                                type: 2
                            },
                            {
                                name:'Острый',
                                type: 3
                            },
                            {
                                name:'С мясом',
                                type: 4
                            },
                            {
                                name:'Хит',
                                type: 5
                            },
                            {
                                name:'Новинки',
                                type: 6
                            },
                            {
                                name:'Соус',
                                type: 7
                            },
                            {
                                name:'Фруктовый',
                                type: 8
                            },
                            {
                                name:'Сливочный',
                                type: 9
                            }
                        ],
                        products:[
                            {
                                img: '/assets/images/products/pirogs/product.png',
                                name: 'Фруктовый (большой)',
                                price: 2000,
                                count: 1,
                                flags: ['Хит']
                            },
                            {
                                img: '/assets/images/products/pirogs/product1.png',
                                name: 'Фруктовый (маленький)',
                                price: 1000,
                                count: 1,
                                flags: []
                            },
                            {
                                img: '/assets/images/products/pirogs/product2.png',
                                name: 'Пирамида',
                                price: 6000,
                                count: 1,
                                flags: ['Новинка']
                            }
                        ]
                    },
                    {
                        heading: 'Выпечка',
                        filters:[
                            {
                                name:'С курицей',
                                type: 1
                            },
                            {
                                name:'Вегетерианский',
                                type: 2
                            },
                            {
                                name:'Острый',
                                type: 3
                            },
                            {
                                name:'С мясом',
                                type: 4
                            },
                            {
                                name:'Хит',
                                type: 5
                            },
                            {
                                name:'Новинки',
                                type: 6
                            },
                            {
                                name:'Соус',
                                type: 7
                            },
                            {
                                name:'Фруктовый',
                                type: 8
                            },
                            {
                                name:'Сливочный',
                                type: 9
                            }
                        ],
                        products:[
                            {
                                img: '/assets/images/products/vipechka/product.png',
                                name: ' Самса с мясом',
                                price: 200,
                                count: 1,
                                flags: ['Хит']
                            },
                            {
                                img: '/assets/images/products/vipechka/product1.png',
                                name: 'Самса с курицей',
                                price: 10000,
                                count: 1,
                                flags: []
                            },
                            {
                                img: '/assets/images/products/vipechka/product2.png',
                                name: 'Борек с мясом',
                                price: 3000,
                                count: 1
                            },
                            {
                                img: '/assets/images/products/vipechka/product3.png',
                                name: 'Борек с картошкой',
                                price: 5000,
                                count: 1,
                            }
                        ]
                    },
                    {
                        heading: 'Пирожное',
                        filters:[
                            {
                                name:'С курицей',
                                type: 1
                            },
                            {
                                name:'Вегетерианский',
                                type: 2
                            },
                            {
                                name:'Острый',
                                type: 3
                            },
                            {
                                name:'С мясом',
                                type: 4
                            },
                            {
                                name:'Хит',
                                type: 5
                            },
                            {
                                name:'Новинки',
                                type: 6
                            },
                            {
                                name:'Соус',
                                type: 7
                            },
                            {
                                name:'Фруктовый',
                                type: 8
                            },
                            {
                                name:'Сливочный',
                                type: 9
                            }
                        ],
                        products:[
                            {
                                img: '/assets/images/products/cakes/product.png',
                                name: 'Капкейк',
                                price: 2040,
                                count: 1,
                                flags: ['Хит'],
                                description:'Test test test Test test test Test test test Test test test'
                            },
                            {
                                img: '/assets/images/products/cakes/product1.png',
                                name: 'Картошка',
                                price: 10000,
                                count: 1,
                                flags: []
                            },
                            {
                                img: '/assets/images/products/cakes/product2.png',
                                name: 'Красный бархат',
                                price: 3000,
                                count: 1
                            }
                        ]
                    },
                    {
                        heading: 'Печенье',
                        filters:[
                            {
                                name:'С курицей',
                                type: 1
                            },
                            {
                                name:'Вегетерианский',
                                type: 2
                            },
                            {
                                name:'Острый',
                                type: 3
                            },
                            {
                                name:'С мясом',
                                type: 4
                            },
                            {
                                name:'Хит',
                                type: 5
                            },
                            {
                                name:'Новинки',
                                type: 6
                            },
                            {
                                name:'Соус',
                                type: 7
                            },
                            {
                                name:'Фруктовый',
                                type: 8
                            },
                            {
                                name:'Сливочный',
                                type: 9
                            }
                        ],
                        products:[
                            {
                                img: '/assets/images/products/cookies/product.png',
                                name: 'Whoopie',
                                price: 2040,
                                count: 1,
                                flags: ['Хит']
                            },
                            {
                                img: '/assets/images/products/cookies/product1.png',
                                name: 'Шоколадные',
                                price: 10000,
                                count: 1,
                                flags: []
                            },
                            {
                                img: '/assets/images/products/cookies/product2.png',
                                name: 'Финики с орехами',
                                price: 3000,
                                count: 1
                            }
                        ]
                    },
                    {
                        heading: 'Полуфабрикаты',
                        filters:[
                            {
                                name:'С курицей',
                                type: 1
                            },
                            {
                                name:'Вегетерианский',
                                type: 2
                            },
                            {
                                name:'Острый',
                                type: 3
                            },
                            {
                                name:'С мясом',
                                type: 4
                            },
                            {
                                name:'Хит',
                                type: 5
                            },
                            {
                                name:'Новинки',
                                type: 6
                            },
                            {
                                name:'Соус',
                                type: 7
                            },
                            {
                                name:'Фруктовый',
                                type: 8
                            },
                            {
                                name:'Сливочный',
                                type: 9
                            }
                        ],
                        products:[
                            {
                                img: '/assets/images/products/polufabricats/product.png',
                                name: 'Бөрек етпен',
                                price: 13700,
                                count: 1,
                                flags: ['Хит']
                            },
                            {
                                img: '/assets/images/products/polufabricats/product1.png',
                                name: 'Пельмени',
                                price: 2500,
                                count: 1,
                                flags: []
                            },
                            {
                                img: '/assets/images/products/polufabricats/product2.png',
                                name: 'Самса тауық етімен ',
                                price: 3000,
                                count: 1
                            }
                        ]
                    }
                ],
                selectedProduct: null,
                cart: []
            }
        },
        methods: {
            showProduct(product){
                this.selectedProduct = product
            },
            addToCart (product) {
                this.cart.push(product)
            },
            removeFromCart(id) {
                console.log(id);
                this.cart.splice(id, 1)
            },
            hideProductModal(){
                if(this.showModal){
                    this.showModal = false;
                }
            },
            clearCart () {
                this.cart = []
            },
            toggleMenu(type){
                if(type ==='user'){
                    this.cartMenu = false;
                    this.userMenu = !this.userMenu;
                } else {
                    this.userMenu = false;
                    this.cartMenu = !this.cartMenu;
                }
            },
            closeUserMenu(){
                this.userMenu = false;
            },
            closeCartMenu(){
                this.cartMenu = false;
            }
        },

    })


// Custom directive to close header menu
    Vue.directive('click-outside', {
        bind: function (el, binding, vnode) {
            el.clickOutsideEvent = function (event) {
                // here I check that click was outside the el and his childrens
                if (!(el == event.target || el.contains(event.target))) {
                    // and if it did, call method provided in attribute value
                    vnode.context[binding.expression](event)
                }
            }
            document.body.addEventListener('click', el.clickOutsideEvent)
        },
        unbind: function (el) {
            document.body.removeEventListener('click', el.clickOutsideEvent)
        }
    })

    //Slick js Carousel
    $('.tatti_banner_carousel').slick({
        dots: true,
        arrows: true,
        speed: 300,
        slidesToShow: 1,
        infinite: true,
        adaptiveHeight: true,
        autoplay: true,
        prevArrow:"<img class='a-left control-c prev slick-prev' src='/assets/icons/arrow_left.svg'>",
        nextArrow:"<img class='a-right control-c next slick-next' src='/assets/icons/arrow_right.svg'>",
    });

    $('.menu_list_carousel').slick({
        dots: false,
        arrows: true,
        speed: 300,
        slidesToShow: 3,
        infinite: true,
        adaptiveHeight: true,
        prevArrow:"<img class='a-left control-c prev slick-prev slick-prev-menu' src='/assets/icons/arr_left.svg'>",
        nextArrow:"<img class='a-right control-c next slick-next slick-next-menu' src='/assets/icons/arr_right.svg'>",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            }]
    });

    $(".hamburger").on("click", function (e) {
        e.preventDefault();
        $(this).toggleClass("is-active");
        if ($(".hamburger").hasClass("is-active")) {
            $(".main-menu").addClass("main-mnu-active");
        } else {
            $(".main-menu").removeClass("main-mnu-active");
        }
    });

    $(".main-menu ul li a").on("click", function (e) {
        $(".hamburger").removeClass("is-active");
        $(".main-menu").removeClass("main-mnu-active");
    });


});




